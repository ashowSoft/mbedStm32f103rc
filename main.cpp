#include "mbed.h"

DigitalOut myled(PA_8);
DigitalOut myLed2(PD_2);
Serial debugSerial(PA_9, PA_10);//tx, rx

int totalCnt = 0;

//串口中断处理函数
void handleSerialRx(){
    if(debugSerial.readable()){
        totalCnt++;
    }
}
int main() {
    //设置波特率
    debugSerial.baud(115200);
    debugSerial.printf("hello\r\n");
    
    //注册中断处理函数
    debugSerial.attach(handleSerialRx);
    
    while(1) {
        debugSerial.printf("totalCnt = %d\r\n",totalCnt);
        
        myLed2 = 0;
        myled = 1; // LED is ON
      
        wait(1); // 200 ms
      
        myLed2 = 1;
        myled = 0; // LED is OFF
        
        wait(1); // 1 sec
    }
}
